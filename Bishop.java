package assignment1;

import java.awt.Point;

public class Bishop extends Piece {
	// Constructors
	public Bishop(String name, String team, int x, int y) {
		super(name, team, x, y);
	}

	public Bishop(Piece piece) {
		super(piece);
	}

	// Methods

	public boolean canMoveTo(Point destination) {

		boolean canMove = false;

		if (!Board.isInBoard(destination) || !MovingDiagonally(destination))
			return false;

		int horizontalDirection = (destination.x > this.location.x ? 1 : -1);
		int verticalDirection = (destination.y > this.location.y ? 1 : -1);

		for (int i = 1; i < 8; i++) {

			Point PointInQuestion = new Point(this.location.x + i * horizontalDirection,
					this.location.y + i * verticalDirection);

			if (Board.isCellOccupied(PointInQuestion) && !PointInQuestion.equals(destination)) {
				canMove = false;
				break;
			} else if (Board.isCellOccupied(PointInQuestion) && PointInQuestion.equals(destination)) {
				canMove = Board.canAttack(this, PointInQuestion);
				break;
			} else if (!Board.isCellOccupied(PointInQuestion) && PointInQuestion.equals(destination)) {
				canMove = true;
				break;
			} else
				continue;
		}

		return canMove;
	}

	private boolean MovingDiagonally(Point destination) {
		if (destination.x == this.location.x || destination.y == this.location.y)
			return false;
		else if (DistanceBetween(destination.x, this.location.x) != DistanceBetween(destination.y, this.location.y))
			return false;
		else
			return true;
	}

	private double DistanceBetween(double source, double dest) {
		return Math.abs(source - dest);
	}
}
