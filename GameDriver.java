package assignment1;

import java.util.Scanner;
import java.awt.Point;

public class GameDriver {
    public static Game game = new Game();
    private static boolean gameEnded = false;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[] move;

        game.SetupGameBoard();
        game.PrintGameBoard();
        game.CurrentTeamsTurn = "white";

        while (!gameEnded) {
            move = GetUserMove(game.CurrentTeamsTurn);
            boolean validMove = game.AttemptTurn(move);
            if (!validMove) {
                System.out.println("Can't do that!");
                continue;
            } else if (game.AcheivedCheckmate())
                break;

            game.PrintGameBoard();
            game.SwitchTeam();
        }

        System.out.println("\n\n-----Game Over!-----\n\n");
        System.out.println("Congratulations to " + game.CurrentTeamsTurn + "! You Won!");
        scanner.close();
    }

    public static void SimulateGame(String[] moves) {
        game.SetupGameBoard();
        game.PrintGameBoard();
        game.CurrentTeamsTurn = "white";

        for (String m : moves) {
            String[] move = m.split(" ");
            game.AttemptTurn(move);
            game.PrintGameBoard();
            game.SwitchTeam();
        }
    }

    private static String[] GetUserMove(String team) {

        System.out.println("Please enter move " + team + ": ");
        String[] move = scanner.nextLine().split(" ");
        if (move.length == 2 && move[0].length() == 2 && move[1].length() == 2) {
            return move;
        } else {
            System.out.println("Invalid input");
            return GetUserMove(team);
        }
    }

    public static Piece returnPiece(String move) {
        try {
            Point thisPoint = game.ConvertToPoint(move);
            return game.gameBoard.getPieceAt(thisPoint);
        } catch (Exception e) {
            return null;
        }
    }

    public static String ConvertPointToString(Point point) {
        String x = "";
        switch (point.x) {
            case 0:
                x = "A";
                break;
            case 1:
                x = "B";
                break;
            case 2:
                x = "C";
                break;
            case 3:
                x = "D";
                break;
            case 4:
                x = "E";
                break;
            case 5:
                x = "F";
                break;
            case 6:
                x = "G";
                break;
            case 7:
                x = "H";
                break;
            default:
                break;
        }
        String y = Integer.toString(point.y + 1);
        return x + y;
    }
}
