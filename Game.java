package assignment1;

import java.util.ArrayList;
import java.awt.Point;
import java.util.*;

public class Game {
    public Board gameBoard;
    public String CurrentTeamsTurn;
    private static boolean enPassantOccured = false;

    public static void SetEnPassant(boolean toSet) {
        enPassantOccured = toSet;
    }

    public void SetupGameBoard() {
        gameBoard = new Board();
    }

    public void SwitchTeam() {
        CurrentTeamsTurn = (CurrentTeamsTurn == "white") ? "black" : "white";
    }

    public void PrintGameBoard() {
        System.out.println("  A  B  C  D  E  F  G  H");
        for (Integer y = 0; y < 8; y++) {
            for (Integer x = 0; x < 8; x++) {
                PrintCell(x, y);
            }
            System.out.print("\n");
        }
    }

    private void PrintCell(Integer x, Integer y) {
        Point here = new Point(x, y);
        if (gameBoard.getPieceAt(here) != null) {
            String leftHalf = ((x == 0) ? ((Integer) (y + 1)).toString() + "|" : "");
            String rightHalf = gameBoard.getPieceAt(here).getName() + "|";
            System.out.print(leftHalf + rightHalf);
        } else
            System.out.print(((x == 0) ? ((Integer) (y + 1)).toString() + "| " : " ") + " |");
    }

    public boolean AttemptTurn(String[] move) {
        Point source;
        Point destination;
        try {
            source = ConvertToPoint(move[0]);
            destination = ConvertToPoint(move[1]);
        } catch (Exception e) {
            return false;
        }

        Piece MovingPiece = Board.getPieceAt(source);
        if (BelongsToTeam(MovingPiece, CurrentTeamsTurn) && MovingPiece.canMoveTo(destination)) {
            MakeMove(MovingPiece, source, destination);
            return true;
        } else
            return false;
    }

    private void MakeMove(Piece toMove, Point source, Point destination) {
        Board.takeCell(toMove, destination);
        if (enPassantOccured) {
            int direction = (CurrentTeamsTurn.equalsIgnoreCase("white") ? 1 : -1);
            Board.removePiece(destination.x + direction, destination.y);
        }
        toMove.setLocation(destination);
        toMove.hasMoved = true;
    }

    public Point ConvertToPoint(String point) throws Exception {
        String x = point.split("")[0].toUpperCase();
        String y = point.split("")[1];
        switch (x.toUpperCase()) {
            case "A":
                x = "1";
                break;
            case "B":
                x = "2";
                break;
            case "C":
                x = "3";
                break;
            case "D":
                x = "4";
                break;
            case "E":
                x = "5";
                break;
            case "F":
                x = "6";
                break;
            case "G":
                x = "7";
                break;
            case "H":
                x = "8";
                break;
            default:
                throw new Exception("Invalid Move!");
        }
        ArrayList<String> list = new ArrayList<String>(Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8"));
        if (!list.contains(y))
            throw new Exception("Invalid Move!");

        return new Point(Integer.parseInt(x) - 1, Integer.parseInt(y) - 1);
    }

    private boolean BelongsToTeam(Piece piece, String team) {
        if (piece != null && piece.getTeam().equalsIgnoreCase(team)) {
            return true;
        } else
            return false;
    }

    public boolean AcheivedCheckmate() {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                Point here = new Point(x, y);
                Piece piece = Board.getPieceAt(here);
                if (piece == null || !piece.getName().contains("K"))
                    continue;

                King thisKing = (King) piece;
                ArrayList<Point> moves = thisKing.getPossibleMoves();

                if (moves.size() != 0 || !thisKing.IsCheck(thisKing.location))
                    continue;

                if (canBlockAttackOn(thisKing))
                    continue;
                else
                    return true; // Checkmate, GG!
            }
        }
        return false;
    }

    private boolean canBlockAttackOn(King king) {
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                Point here = new Point(x, y);
                Piece attacker = Board.getPieceAt(here);
                if (attacker == null || attacker.getTeam().equalsIgnoreCase(king.getTeam()))
                    continue;

                ArrayList<Point> moves = attacker.getPossibleMoves();

                if (moves.contains(king.getLocation())) {
                    if (attacker.getName().contains("N"))
                        return false;
                    if (canBlockAttacker(attacker, king.getLocation()))
                        return true;
                }
            }
        }
        return false;
    }

    private boolean canBlockAttacker(Piece attacker, Point kingsLocation) {
        ArrayList<Point> attackingPath = getPathTo(attacker, kingsLocation);

        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                Piece possibleBlocker = Board.getPieceAt(x, y);
                if (possibleBlocker == null || possibleBlocker.getTeam().equalsIgnoreCase(attacker.getTeam()))
                    continue;
                ArrayList<Point> moves = possibleBlocker.getPossibleMoves();

                for (Point move : moves) {
                    if (attackingPath.contains(move))
                        return true;
                }
            }
        }
        return false;
    }

    private ArrayList<Point> getPathTo(Piece piece, Point here) {
        ArrayList<Point> path = new ArrayList<Point>();

        int horizontalDistance = Math.abs(piece.getLocation().x - here.x) - 1;
        int verticalDistance = Math.abs(piece.getLocation().y - here.y) - 1;

        int horizontalDirection = (piece.getLocation().x > here.x ? -1 : 1);
        int verticalDirection = (piece.getLocation().y > here.y ? -1 : 1);

        int x = piece.getLocation().x;
        int y = piece.getLocation().y;
        while (horizontalDistance != -1 && verticalDistance != -1) {
            Point loc = new Point(x + horizontalDistance * horizontalDirection,
                    y + verticalDistance * verticalDirection);
            if (piece.canMoveTo(loc) || loc.equals(piece.getLocation())) {
                path.add(loc);
            }
            if (horizontalDistance != -1)
                horizontalDistance--;
            if (verticalDistance != -1)
                verticalDistance--;
        }

        return path;
    }

}
