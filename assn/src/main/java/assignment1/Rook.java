package assignment1;

import java.awt.Point;

public class Rook extends Piece {
	// Constructors
	public Rook(String name, String team, int x, int y) {
		super(name, team, x, y);
	}

	public Rook(Piece piece) {
		super(piece);
	}

	// Methods

	public boolean canMoveTo(int newX, int newY) {
		Point destination = new Point(newX, newY);
		boolean canMove;
		if (!Board.isInBoard(destination) || this.location.equals(destination))
			canMove = false;
		else if (destination.x == this.location.x)
			canMove = canRookMoveVertically(destination);
		else if (destination.y == this.location.y)
			canMove = canRookMoveHorizontally(destination);
		else
			canMove = false;

		return canMove;
	}

	private boolean canRookMoveVertically(Point destination) {
		int direction = (destination.y > this.location.y ? 1 : -1);
		int spacesToMove = Math.abs(destination.y - this.location.y);
		for (int distance = 1; distance <= spacesToMove; distance++) {
			Piece onThisCell = Board.getPieceAt(this.location.x, this.location.y + distance * direction);
			if (onThisCell != null) {
				if (distance != spacesToMove)
					return false;
				else if (onThisCell.getTeam().equalsIgnoreCase(this.team))
					return false;
				else
					return true;
			}
		}
		return true;
	}

	private boolean canRookMoveHorizontally(Point destination) {
		int direction = (destination.x > this.location.x ? 1 : -1);
		int spacesToMove = Math.abs(destination.x - this.location.x);
		for (int distance = 1; distance <= spacesToMove; distance++) {
			Piece onThisCell = Board.getPieceAt(this.location.x + distance * direction, this.location.y);
			if (onThisCell != null) {
				if (distance != spacesToMove)
					return false;
				else if (onThisCell.getTeam().equalsIgnoreCase(this.team))
					return false;
				else
					return true;
			}
		}
		return true;
	}
}
