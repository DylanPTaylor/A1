package assignment1;

import java.awt.Point;

public class Cell {
    private Piece PieceOnCell = null;
    private boolean isOccupied;
    private Point coordinate; // Immutable once declared

    private Cell() {
    } // Force 1 or 2 arguments

    public Cell(Point coord) {
        this.coordinate = coord;
        this.PieceOnCell = null;
        this.isOccupied = false;
    }

    public Cell(Point coord, Piece piece) {
        this.PieceOnCell = piece;
        this.coordinate = coord;
        this.isOccupied = true;
    }

    public Point getCoordinates() {
        return this.coordinate;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setOccupied(boolean toSet) {
        isOccupied = toSet;
    }

    public Piece getOccupyingPiece() {
        return PieceOnCell;
    }

    public Piece setPieceOnCell(Piece toSet) {
        Piece removedPiece = null;
        if (isOccupied) {
            removedPiece = PieceOnCell;
            PieceOnCell = toSet;
        } else
            PieceOnCell = toSet;
        if (toSet == null)
            setOccupied(false);
        else
            setOccupied(true);
        return removedPiece;
    }
}