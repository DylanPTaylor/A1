package assignment1;

import java.awt.Point;

public class Board {
  // Instance Variables
  private static Cell[][] gameBoard;

  public Board() {
    gameBoard = new Cell[8][8];
    for (int x = 0; x < 8; x++) {
      for (int y = 0; y < 8; y++) {
        gameBoard[x][y] = new Cell(new Point(x, y));
      }
    }

    // Create Piece Objects // declare white side of the gameBoard
    setPieceAt(new Point(1, 0), new Pawn("WP", "white", 1, 0));
    setPieceAt(new Point(1, 1), new Pawn("WP", "white", 1, 1));
    setPieceAt(new Point(1, 2), new Pawn("WP", "white", 1, 2));
    setPieceAt(new Point(1, 3), new Pawn("WP", "white", 1, 3));
    setPieceAt(new Point(1, 4), new Pawn("WP", "white", 1, 4));
    setPieceAt(new Point(1, 5), new Pawn("WP", "white", 1, 5));
    setPieceAt(new Point(1, 6), new Pawn("WP", "white", 1, 6));
    setPieceAt(new Point(1, 7), new Pawn("WP", "white", 1, 7));

    setPieceAt(new Point(0, 0), new Rook("WR", "white", 0, 0));
    setPieceAt(new Point(0, 1), new Knight("WN", "white", 0, 1));
    setPieceAt(new Point(0, 2), new Bishop("WB", "white", 0, 2));
    setPieceAt(new Point(0, 3), new King("WK", "white", 0, 3));
    setPieceAt(new Point(0, 4), new Queen("WQ", "white", 0, 4));
    setPieceAt(new Point(0, 5), new Bishop("WB", "white", 0, 5));
    setPieceAt(new Point(0, 6), new Knight("WN", "white", 0, 6));
    setPieceAt(new Point(0, 7), new Rook("WR", "white", 0, 7));

    // declare black side of gameBoard
    setPieceAt(new Point(6, 0), new Pawn("BP", "black", 6, 0));
    setPieceAt(new Point(6, 1), new Pawn("BP", "black", 6, 1));
    setPieceAt(new Point(6, 2), new Pawn("BP", "black", 6, 2));
    setPieceAt(new Point(6, 3), new Pawn("BP", "black", 6, 3));
    setPieceAt(new Point(6, 4), new Pawn("BP", "black", 6, 4));
    setPieceAt(new Point(6, 5), new Pawn("BP", "black", 6, 5));
    setPieceAt(new Point(6, 6), new Pawn("BP", "black", 6, 6));
    setPieceAt(new Point(6, 7), new Pawn("BP", "black", 6, 7));

    setPieceAt(new Point(7, 0), new Rook("BR", "black", 7, 0));
    setPieceAt(new Point(7, 1), new Knight("BN", "black", 7, 1));
    setPieceAt(new Point(7, 2), new Bishop("BB", "black", 7, 2));
    setPieceAt(new Point(7, 3), new King("BK", "black", 7, 3));
    setPieceAt(new Point(7, 4), new Queen("BQ", "black", 7, 4));
    setPieceAt(new Point(7, 5), new Bishop("BB", "black", 7, 5));
    setPieceAt(new Point(7, 6), new Knight("BN", "black", 7, 6));
    setPieceAt(new Point(7, 7), new Rook("BR", "black", 7, 7));
  }

  // setters and getters
  private static Cell getCellAt(Point thisPoint) {
    return gameBoard[thisPoint.x][thisPoint.y];
  }

  public static Piece getPieceAt(Point thisPoint) {
    return getCellAt(thisPoint).getOccupyingPiece();
  }

  public static Piece getPieceAt(int x, int y) {
    return getCellAt(new Point(x, y)).getOccupyingPiece();
  }

  public static Piece setPieceAt(Point thisPoint, Piece toSet) {
    Piece safeToSet = toSet;
    return getCellAt(thisPoint).setPieceOnCell(safeToSet);
  }

  // checks to see if there is a piece at x,y
  public static boolean isCellOccupied(Point thisPoint) {
    return getCellAt(thisPoint).isOccupied();
  }

  public static boolean isCellOccupied(int x, int y) {
    return getCellAt(new Point(x, y)).isOccupied();
  }

  public static Piece takeCell(Piece MovingPiece, Point thisPoint) {
    Piece removed = getPieceAt(thisPoint);
    setPieceAt(MovingPiece.getLocation(), null);
    setPieceAt(thisPoint, MovingPiece);
    return removed;
  }

  public static boolean canAttack(Piece attackingPiece, Point thisPoint) {
    if (isCellOccupied(thisPoint))
      return !getPieceAt(thisPoint).getTeam().equalsIgnoreCase(attackingPiece.getTeam());
    else
      return false;
  }

  public static boolean canMove(Piece toMove, Point thisPoint) {
    if (isCellOccupied(thisPoint))
      return canAttack(toMove, thisPoint);
    else
      return true;
  }

  public static void removePiece(int x, int y) {
    setPieceAt(new Point(x, y), null);
  }

  public static boolean isInBoard(Point position) {
    if (position.x > -1 && position.y > -1 && position.x < 8 && position.y < 8)
      return true;
    return false;
  }

  // For AI (may not keep)
  public static float evaluateBoard() {
    float sum = 0;
    for (int x = 0; x < 8; x++) {
      for (int y = 0; y < 8; y++) {
        if (Board.isCellOccupied(x, y)) {
          sum += getPieceAt(x, y).getValue();
        }
      }
    }
    return sum;
  }
}
