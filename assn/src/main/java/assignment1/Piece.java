package assignment1;

import java.util.ArrayList;
import java.awt.Point;

public abstract class Piece {
	// Instance Variables
	protected String name;
	protected String team;
	protected boolean onBoard;
	protected Point location;

	// Constructors
	public Piece(Piece toCopy) {
		name = toCopy.getName();
		team = toCopy.getTeam();
		onBoard = toCopy.isOnBoard();
		location = toCopy.getLocation();
	}

	public Piece(String name, String team, int x, int y) {
		this.name = new String(name);
		this.team = new String(team);
		onBoard = true;
		location = new Point(x, y);
	}

	// Getters
	public Point getLocation() {
		Point point = location;
		return point;
	}

	public String getName() {
		String returnName = new String(name);
		return returnName;
	}

	public String getTeam() {
		String returnTeam = new String(team);
		return returnTeam;
	}

	public boolean isOnBoard() {
		return onBoard;
	}
	// Setters

	public void setLocation(Point point) {
		Point p = point;
		location = p;
	}

	// Methods

	/**
	 * canMoveTo() checks that all the conditions of a move, based on the Piece
	 * type, are satisfied
	 */
	public abstract boolean canMoveTo(Point destination);

	public ArrayList<Point> getPossibleMoves() {
		ArrayList<Point> PossibleMoves = new ArrayList<Point>();
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (this.canMoveTo(new Point(x, y)))
					PossibleMoves.add(new Point(x, y));
			}
		}
		return PossibleMoves;
	}
