package assignment1;

import java.awt.Point;

public class Knight extends Piece {

	// Constructors
	public Knight(String name, String team, int x, int y) {
		super(name, team, x, y);
	}

	public Knight(Piece piece) {
		super(piece);
	}

	// Methods
	public boolean canMoveTo(int newX, int newY) {
		Point destination = new Point(newX, newY);
		if (!Board.isInBoard(destination))
			return false;

		int horizontalDirection = (destination.x > this.location.x ? 1 : -1);
		int verticalDirection = (destination.y > this.location.y ? 1 : -1);

		Point validDestination1 = new Point(this.location.x + 2 * horizontalDirection,
				this.location.y + 1 * verticalDirection);
		Point validDestination2 = new Point(this.location.x + 1 * horizontalDirection,
				this.location.y + 2 * verticalDirection);

		if (destination.equals(validDestination1) || destination.equals(validDestination2)) {
			if (Board.canMove(this, destination))
				return true;
		}
		return false;
	}
}