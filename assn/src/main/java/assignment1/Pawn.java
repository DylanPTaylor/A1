package assignment1;

import java.awt.Point;

public class Pawn extends Piece {
	// Instance variables
	private int movingDirection;
	private boolean AttackableEnPassant = false;

	private boolean hasMoved = false;

	// Constructors
	public Pawn(String name, String team, int x, int y) {
		super(name, team, x, y);
		if (team.equalsIgnoreCase("black")) {
			movingDirection = -1;
		} else
			movingDirection = 1;
	}

	public Pawn(Piece toCopy) {
		super(toCopy);
		if (team.equalsIgnoreCase("black"))
			movingDirection = -1;
		else
			movingDirection = 1;
	}

	// Getters/Setters
	// Note: En Passant means "In passing" in french
	public boolean AttackableEnPassant() {
		return AttackableEnPassant;
	}

	// Methods

	public boolean canMoveTo(int newX, int newY) {
		Point destination = new Point(newX, newY);
		boolean canMove;
		if (!Board.isInBoard(destination))
			canMove = false;
		else if (AttemptingEnPassant(destination))
			canMove = enPassant(destination);
		else if (pawnIsMovingForward(destination))
			canMove = true;
		else if (pawnIsAttacking(destination))
			canMove = true;
		else
			canMove = false;

		if (canMove)
			this.hasMoved = true;
		return canMove;
	}

	public boolean pawnIsMovingForward(Point destination) {
		if (!Board.isCellOccupied(destination) && destination.y == this.location.y) {
			// There is no piece here, so we can move here
			// The pawn is trying to move 2 squares, can only do so on first move.
			if (destination.x == this.location.x + (2 * movingDirection) && !hasMoved) {
				// Cannot jump over a piece
				if (Board.isCellOccupied(this.location.x + movingDirection, this.location.y))
					return false;
				else {
					AttackableEnPassant = true;
					return true;
				}
			}
			// The pawn is trying to move one square
			else if (destination.x == this.location.x + movingDirection) {
				AttackableEnPassant = false;
				return true;
			}
		}
		return false;
	}

	public boolean pawnIsAttacking(Point destination) {
		// Make sure pawn is only trying to move one column over
		if (destination.y == this.location.y + 1 || destination.y == this.location.y - 1) {
			// Make sure pawn is moving 1 square forward
			if (destination.x == this.location.x + movingDirection)
				// Make sure destination has an enemy piece occupying it
				return Board.canAttack(this, destination);
		}
		return false;
	}

	private boolean AttemptingEnPassant(Point destination) {
		if (this.team.equalsIgnoreCase("white")) {
			if (this.location.x == 4 && destination.x == 5
					&& (this.location.y == destination.y + 1 || this.location.y == destination.y - 1)) {
				// The direction is valid
				if (!Board.isCellOccupied(destination)) {
					return true;
				}
			}
		}
		// Otherwise the team is black
		else if (this.location.x == 3 && destination.x == 2
				&& (this.location.y == destination.y + 1 || this.location.y == destination.y - 1)) {
			// The direction is valid
			if (!Board.isCellOccupied(destination)) {
				return true;
			}
		}
		return false;
	}

	private boolean enPassant(Point destination) {
		// If this code is reached, AttemptingEnPassant has returned true, so we know
		// the move is valid
		// Make sure there is a pawn from the other team there.
		Piece victim = Board.getPieceAt(destination.x - movingDirection, destination.y);
		if (victim == null || victim.getTeam().equalsIgnoreCase(this.team) || !victim.getName().equalsIgnoreCase("P"))
			return false;
		Pawn victimPawn = (Pawn) victim;
		if (victimPawn.AttackableEnPassant()) {
			Game.SetEnPassant(true);
			return true;
		}
		return false;
	}
}
