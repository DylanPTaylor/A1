package assignment1;

import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

import java.awt.Point;

public class KnightTest {
    GameDriver driver = new GameDriver();

    @Test
    public void MovesKnights() {
        String[] moves = { "A7 C6", "H2 F3", "C6 D4", "F3 E5" };

        driver.SimulateGame(moves);

        Piece D4 = driver.returnPiece("D4");
        Piece E5 = driver.returnPiece("E5");

        Assert.assertTrue(D4.getName().equalsIgnoreCase("WN") && E5.getName().equalsIgnoreCase("BN"));
    }

    @Test
    public void TryInvalidMoves() {
        String[] invalidDestination = { "A5", "C7", "D7", "B5", "B7" };
        for (int i = 0; i < invalidDestination.length; i++) {
            String move = "A7 " + invalidDestination[i];
            String[] moves = { move };
            driver.SimulateGame(moves);

            Piece A7 = driver.returnPiece("A7");

            Assert.assertTrue(A7.getName().equalsIgnoreCase("WN"));
        }
    }

    @Test
    public void TryAllValidWhiteFromCentre() {
        String[] setupMoves = { "A7 C6", "G8 F8", "C6 E5" };

        driver.SimulateGame(setupMoves);

        Piece knight = driver.returnPiece("E5");
        ArrayList<Point> points = knight.getPossibleMoves();

        ArrayList<Point> validDestinations = new ArrayList<Point>();

        validDestinations.add(new Point(2, 3));
        validDestinations.add(new Point(2, 5));
        validDestinations.add(new Point(3, 2));
        validDestinations.add(new Point(3, 6));
        validDestinations.add(new Point(5, 2));
        validDestinations.add(new Point(5, 6));
        validDestinations.add(new Point(6, 3));
        validDestinations.add(new Point(6, 5));

        for (int i = 0; i < points.size(); i++) {
            System.out.println(points.get(i));
            assertTrue(validDestinations.contains(points.get(i)));
        }
    }

    @Test
    public void TryAllValidBlackFromCentre() {
        String[] setupMoves = { "B8 C8", "H2 F3", "C8 D8", "F3 D4" };

        driver.SimulateGame(setupMoves);

        Piece knight = driver.returnPiece("D4");
        ArrayList<Point> points = knight.getPossibleMoves();

        ArrayList<Point> validDestinations = new ArrayList<Point>();

        validDestinations.add(new Point(1, 2));
        validDestinations.add(new Point(1, 4));
        validDestinations.add(new Point(2, 1));
        validDestinations.add(new Point(2, 5));
        validDestinations.add(new Point(4, 1));
        validDestinations.add(new Point(4, 5));
        validDestinations.add(new Point(5, 2));
        validDestinations.add(new Point(5, 4));

        for (int i = 0; i < points.size(); i++) {
            System.out.println(points.get(i));
            assertTrue(validDestinations.contains(points.get(i)));
        }
    }
}
