package assignment1;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

import java.awt.Point;

public class KingTest {
    GameDriver driver = new GameDriver();

    @Test
    public void TestMovesFromCentre() {
        String[] moves = { "B5 D5", "G5 E5", "A4 B5", "H4 G5" };

        driver.SimulateGame(moves);

        Piece B5 = driver.returnPiece("B5");
        Piece G5 = driver.returnPiece("G5");

        Assert.assertTrue(B5.getName().equalsIgnoreCase("WK") && G5.getName().equalsIgnoreCase("BK"));

        ArrayList<Point> B5Moves = B5.getPossibleMoves();

        ArrayList<Point> G5Moves = G5.getPossibleMoves();

        ArrayList<Point> B5ValidMoves = new ArrayList<Point>();
        ArrayList<Point> G5ValidMoves = new ArrayList<Point>();

        B5ValidMoves.add(new Point(0, 3));
        B5ValidMoves.add(new Point(2, 3));
        B5ValidMoves.add(new Point(2, 4));
        B5ValidMoves.add(new Point(2, 5));

        G5ValidMoves.add(new Point(7, 3));
        G5ValidMoves.add(new Point(5, 3));
        G5ValidMoves.add(new Point(5, 4));
        G5ValidMoves.add(new Point(5, 5));

        for (Point p : B5Moves) {
            assertTrue("Point " + p.toString() + " was invalid for B5", B5ValidMoves.contains(p));
        }
        for (Point p : G5Moves) {
            assertTrue("Point " + p.toString() + " was invalid for G5", G5ValidMoves.contains(p));
        }
    }

    @Test
    public void TestMovingIntoCheck() {
        String[] moves = { "B5 D5", "G5 E5", "A4 B5", "H4 G5", "B5 C4", "G5 F6", "C4 D4", "F6 E6" };

        driver.SimulateGame(moves);

        Piece C4 = driver.returnPiece("C4");
        Piece F6 = driver.returnPiece("F6");

        Assert.assertTrue(C4.getName().equalsIgnoreCase("WK") && F6.getName().equalsIgnoreCase("BK"));
    }

    @Test
    public void TestMovingIntoCheck2() {
        String[] moves = { "B5 D5", "G5 E5", "A4 B5", "H5 G5", "B5 C5", "G5 F6", "C5 C6" };

        driver.SimulateGame(moves);

        Piece C5 = driver.returnPiece("C5");

        Assert.assertTrue(C5.getName().equalsIgnoreCase("WK"));
    }

    @Test
    public void TestFoolsMate() {
        String[] moves = { "B3 C3", "G4 F4", "B2 D2", "H5 D1" };

        driver.SimulateGame(moves);

        assertTrue(driver.game.AcheivedCheckmate());
    }

    @Test
    public void TestScholarsMate() {
        String[] moves = { "B4 D4", "G4 E4", "A3 D6", "H7 F6", "A5 E1", "H2 F3", "E1 G3" };

        driver.SimulateGame(moves);

        assertTrue(driver.game.AcheivedCheckmate());
    }

    @Test
    public void TestHippoMate() {
        String[] moves = { "B4 D4", "G4 E4", "A2 B4", "H5 D1", "A7 C6", "h7 f6", "b2 c2", "D1 E2", "b5 d5", "f6 d5",
                "A6 E2", "D5 C3" };

        driver.SimulateGame(moves);

        assertTrue(driver.game.AcheivedCheckmate());
    }

}
