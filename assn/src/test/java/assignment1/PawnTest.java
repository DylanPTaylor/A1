package assignment1;

import org.junit.Assert;
import org.junit.Test;

public class PawnTest {
    GameDriver driver = new GameDriver();

    @Test
    public void WhitePawnTakesBlack() {
        String[] moves = { "B1 D1", "G2 E2", "D1 E2" };
        driver.SimulateGame(moves);

        Piece E2 = driver.returnPiece("E2");

        Assert.assertTrue(E2.getName().equalsIgnoreCase("WP"));
    }

    @Test
    public void BlackPawnTakesWhite() {
        String[] moves = { "B5 D5", "G6 F6", "D5 E5", "F6 E5" };

        driver.SimulateGame(moves);

        Piece E5 = driver.returnPiece("E5");

        Assert.assertTrue(E5.getName().equalsIgnoreCase("BP"));
    }

    @Test
    public void PawnAttemptsAttackForward() {
        String[] moves = { "B5 D5", "G5 E5", "D5 E5" };

        driver.SimulateGame(moves);

        Piece E5 = driver.returnPiece("E5");
        Piece D5 = driver.returnPiece("D5");

        Assert.assertTrue(E5.getName().equalsIgnoreCase("BP"));
        Assert.assertTrue(D5.getName().equalsIgnoreCase("WP"));
    }
}
