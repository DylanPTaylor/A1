package assignment1;

import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

import java.awt.Point;

public class QueenTest {
    GameDriver driver = new GameDriver();

    @Test
    public void TryInvalidMoves() {
        String[] invalidDestination = { "B2", "F1", "C8" };
        String[] setupMoves = { "B4 C4", "G8 F8", "A5 C3", "F8 E8" };
        for (int i = 0; i < invalidDestination.length; i++) {
            String[] moves = new String[5];

            for (int j = 0; j < 4; j++) {
                moves[j] = setupMoves[j];
            }
            moves[4] = "C3 " + invalidDestination[i];

            driver.SimulateGame(moves);

            Piece C3 = driver.returnPiece("C3");

            Assert.assertTrue(C3.getName().equalsIgnoreCase("WQ"));
        }
    }

    @Test
    public void TryAllValidFromCentre() {
        String[] setupMoves = { "B4 C4", "G8 F8", "A5 D2" };

        driver.SimulateGame(setupMoves);

        Piece queen = driver.returnPiece("D2");
        ArrayList<Point> points = queen.getPossibleMoves();

        ArrayList<Point> validDestinations = new ArrayList<Point>();

        validDestinations.add(new Point(0, 4));

        validDestinations.add(new Point(1, 3));

        validDestinations.add(new Point(2, 0));
        validDestinations.add(new Point(2, 1));
        validDestinations.add(new Point(2, 2));

        validDestinations.add(new Point(3, 0));
        validDestinations.add(new Point(3, 2));
        validDestinations.add(new Point(3, 3));
        validDestinations.add(new Point(3, 4));
        validDestinations.add(new Point(3, 5));
        validDestinations.add(new Point(3, 6));
        validDestinations.add(new Point(3, 7));

        validDestinations.add(new Point(4, 0));
        validDestinations.add(new Point(4, 1));
        validDestinations.add(new Point(4, 2));

        validDestinations.add(new Point(5, 1));
        validDestinations.add(new Point(5, 3));

        validDestinations.add(new Point(6, 1));
        validDestinations.add(new Point(6, 4));

        for (int i = 0; i < points.size(); i++) {
            System.out.println(points.get(i));
            assertTrue(validDestinations.contains(points.get(i)));
        }
    }
}
