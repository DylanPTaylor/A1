package assignment1;

import org.junit.Assert;
import org.junit.Test;

public class RookTest {
    GameDriver driver = new GameDriver();

    @Test
    public void WhiteRookTakesBlack() {
        String[] moves = { "B8 D8", "G1 E1", "A8 C8", "H1 F1", "C8 C4", "F1 F4", "C4 F4" };

        driver.SimulateGame(moves);

        Piece F4 = driver.returnPiece("F4");

        Assert.assertTrue(F4.getName().equalsIgnoreCase("WR"));
    }

    @Test
    public void BlackRookTakesWhitePawn() {
        String[] moves = { "B8 D8", "G1 E1", "B4 D4", "H1 F1", "D4 E4", "F1 F4", "A8 C8", "F4 E4" };

        driver.SimulateGame(moves);

        Piece E4 = driver.returnPiece("E4");

        Assert.assertTrue(E4.getName().equalsIgnoreCase("BR"));
    }

    @Test
    public void BlackRookTakesWhite() {
        String[] moves = { "B8 D8", "G1 E1", "A8 C8", "H1 F1", "C8 C4", "F1 F4", "C4 D4", "F4 D4" };

        driver.SimulateGame(moves);

        Piece D4 = driver.returnPiece("D4");

        Assert.assertTrue(D4.getName().equalsIgnoreCase("BR"));
    }

}
