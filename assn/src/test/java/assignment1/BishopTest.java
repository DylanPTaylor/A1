package assignment1;

import org.junit.Assert;
import org.junit.Test;

public class BishopTest {
    GameDriver driver = new GameDriver();

    @Test
    public void WhiteBishopTakesBlackPawnAndViceVersa() {
        String[] moves = { "B5 D5", "G2 E2", "A6 C4", "H3 G2", "C4 E2", "G2 D5" };

        driver.SimulateGame(moves);

        Piece E2 = driver.returnPiece("E2");
        Piece D5 = driver.returnPiece("D5");
        Assert.assertTrue(E2.getName().equalsIgnoreCase("WB") && D5.getName().equalsIgnoreCase("BB"));
    }

    @Test
    public void WhiteBishopTakesBlawnBishopAndPawn() {
        String[] moves = { "B2 D2", "G5 E5", "A3 B2", "H6 G5", "B2 E5", "G1 F1", "E5 G7", "G5 F6", "G7 F6" };

        driver.SimulateGame(moves);

        Piece F6 = driver.returnPiece("F6");
        Piece G7 = driver.returnPiece("G7");

        Assert.assertTrue(F6.getName().equalsIgnoreCase("WB") && G7 == null);
    }

    @Test
    public void BishopGetsBlocked() {
        String[] moves = { "B2 D2", "G5 E5", "A3 B2", "H6 G5", "B2 G7" };

        driver.SimulateGame(moves);

        Piece B2 = driver.returnPiece("B2");
        Piece G7 = driver.returnPiece("G7");

        Assert.assertTrue(G7.getName().equalsIgnoreCase("BP") && B2.getName().equalsIgnoreCase("WB"));
    }

}
