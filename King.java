package assignment1;

import java.awt.Point;

public class King extends Piece {

	// Constructors
	public King(String name, String team, int x, int y) {
		super(name, team, x, y);
	}

	public King(Piece toCopy) {
		super(toCopy);
	}

	// Methods
	public boolean canMoveTo(Point destination) {
		if (!Board.isInBoard(destination) || destination.equals(this.location))
			return false;

		int horizontalChange = (destination.x == this.location.x ? 0 : 1);
		int verticalChange = (destination.y == this.location.y ? 0 : 1);

		int horizontalDirection = (destination.x > this.location.x ? 1 : -1);
		int verticalDirection = (destination.y > this.location.y ? 1 : -1);

		Point validDestination = new Point(this.location.x + horizontalChange * horizontalDirection,
				this.location.y + verticalChange * verticalDirection);

		if (destination.equals(validDestination) && Board.canMove(this, validDestination))
			return !IsCheck(destination);
		else
			return false;
	}

	public boolean IsCheck(Point destination) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				Piece piece = Board.getPieceAt(x, y);
				if (piece == null || piece.getTeam().equalsIgnoreCase(this.team))
					continue;
				if (piece.getName().contains("P")) {
					if (SimulatePawnAttack(piece, destination))
						return true;
					else
						continue;
				} else {
					Piece temp = Board.getPieceAt(destination);
					Board.setPieceAt(destination, null);
					boolean canMove = piece.canMoveTo(destination);
					Board.setPieceAt(destination, temp);
					if (canMove)
						return canMove;
					else
						continue;
				}
			}
		}
		return false;
	}

	private boolean SimulatePawnAttack(Piece pawn, Point destination) {
		boolean canBeAttacked = false;
		King fakeKing = new King(this.name, this.team, destination.x, destination.y);
		Piece temp = Board.getPieceAt(destination);

		Board.setPieceAt(destination, fakeKing);

		canBeAttacked = pawn.canMoveTo(destination);

		Board.setPieceAt(destination, temp);

		return canBeAttacked;
	}
}
