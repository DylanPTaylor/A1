package assignment1;

import java.awt.Point;

public class Queen extends Piece {
	// Constructors
	public Queen(String name, String team, int x, int y) {
		super(name, team, x, y);
	}

	public Queen(Piece piece) {
		super(piece);
	}

	public boolean canMoveTo(int newX, int newY) {
		Point destination = new Point(newX, newY);
		if (!Board.isInBoard(destination) || this.location.equals(destination))
			return false;

		// Queens can move in the same manner as a rook or bishop, so create
		// fake ones and utilize their canMoveTo function to check validity.
		Bishop fakeBishop = new Bishop("", this.team, this.location.x, this.location.y);
		Rook fakeRook = new Rook("", this.team, this.location.x, this.location.y);

		if (fakeBishop.canMoveTo(destination) || fakeRook.canMoveTo(destination))
			return true;
		return false;
	}
}
